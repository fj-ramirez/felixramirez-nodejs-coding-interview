import { JsonController, Get, Patch, Body, Param } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'
import { PersonsService } from '../services/persons.service'

const flightsService = new FlightsService()
const personsService = new PersonsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Patch('/:id/onboard')
    async modify(@Param('id') flightId: string,@Body() p: any) {
        const person = await personsService.getById(p.userId)
        const flight = await flightsService.getById(flightId)

        flight.passengers.push(person.name) //Should be a reference, but no time for that!

        await flightsService.modify(flight)

        return { status: 201 }
                
    }
}
