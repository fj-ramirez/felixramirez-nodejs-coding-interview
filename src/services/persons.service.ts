import { PersonsModel } from '../models/persons.model'

export class PersonsService {
    async getAll() {
        return await PersonsModel.find()
    }

    async getById(id: string) {
        return await PersonsModel.findById(id)
    }

    async create(person: any) {
        await PersonsModel.create(person)
    }

    async delete(id: any) {
        await PersonsModel.findByIdAndDelete(id)
    }
}
