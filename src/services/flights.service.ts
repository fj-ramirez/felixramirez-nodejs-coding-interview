import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async getById(id: string) {
        return await FlightsModel.findById(id)
    }

    async modify(flight: any) {
        return await FlightsModel.findByIdAndUpdate(flight._id, flight)
    }
}
